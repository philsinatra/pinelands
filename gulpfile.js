var gulp = require('gulp');

// Include Our Plugins
var autoprefixer = require('gulp-autoprefixer'),
  argv = require('yargs').argv,
  del = require('del'),
  header = require('gulp-header'),
  gulpif = require('gulp-if'),
  sass = require('gulp-sass');

var pkg = require('./package.json');
var banner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' */',
  ''
].join('\n');

/*
#    /$$$$$$   /$$$$$$   /$$$$$$   /$$$$$$
#   /$$__  $$ /$$__  $$ /$$__  $$ /$$__  $$
#  | $$  \__/| $$  \__/| $$  \__/| $$  \__/
#  |  $$$$$$ | $$      |  $$$$$$ |  $$$$$$
#   \____  $$| $$       \____  $$ \____  $$
#   /$$  \ $$| $$    $$ /$$  \ $$ /$$  \ $$
#  |  $$$$$$/|  $$$$$$/|  $$$$$$/|  $$$$$$/
#   \______/  \______/  \______/  \______/
#
#
#
*/

var stylesheetHeader = ['/*',
  'Theme Name: Pinelands Repair and Design',
  'Theme URI: http://wordpress.org/themes/twentythirteen',
  'Author: Phil Sinatra',
  'Author URI: http://philsinatra.com',
  'Description: Custom Wordpress Theme template.',
  'Version: 1.0',
  'License: none',
  '*/'
].join('\n');

gulp.task('scss', function() {
  del(['./pinelands-custom/style.css']);
  var outputStyle = argv.production ? 'compressed' : 'nested';

  // Generate stylesheet
  gulp.src('./src/files/scss/style.scss')
    .pipe(sass({
      outputStyle: outputStyle
    }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(header(stylesheetHeader))
    .pipe(gulp.dest('./pinelands-custom/'));
});

/*
#   /$$      /$$             /$$               /$$
#  | $$  /$ | $$            | $$              | $$
#  | $$ /$$$| $$  /$$$$$$  /$$$$$$    /$$$$$$$| $$$$$$$
#  | $$/$$ $$ $$ |____  $$|_  $$_/   /$$_____/| $$__  $$
#  | $$$$_  $$$$  /$$$$$$$  | $$    | $$      | $$  \ $$
#  | $$$/ \  $$$ /$$__  $$  | $$ /$$| $$      | $$  | $$
#  | $$/   \  $$|  $$$$$$$  |  $$$$/|  $$$$$$$| $$  | $$
#  |__/     \__/ \_______/   \___/   \_______/|__/  |__/
#
#
#
*/
gulp.task('watch', function() {
  gulp.watch('./src/files/scss/**/*.scss', ['scss']);
});

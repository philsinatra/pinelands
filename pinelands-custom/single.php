<?php
get_header();
get_template_part('page','top');
?>

<main>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <article>
    <div class="o_container">

      <?php if ( has_post_thumbnail() ) { ?>
      <div class="o_row">
        <div class="c_single_feature_image">
          <figure>
            <?php the_post_thumbnail(); ?>
          </figure>
        </div>
      </div>
      <?php } ?>

      <div class="o_row">
        <div class="c_single_post">
          <h1><?php the_title(); ?></h1>
          <?php the_content(); ?>
        </div>
      </div>
    </div> <!-- /.o_container -->
  </article>

<?php endwhile; else: ?>
  <p>Some error message or similar.</p>
<?php endif; ?>

</main>

<?php get_footer(); ?>

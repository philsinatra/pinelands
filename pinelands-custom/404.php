<?php
get_header();
get_template_part('page','top');
?>

<main>
  <article>
    <div class="o_container">
      <div class="c_404">
        <h1>404</h1>
        <h2>The requested page can not be found.</h2>
        <p>
          Go back or return to the <a href="<?php echo get_option('home'); ?>">">home page</a> to choose a new page.
        </p>
      </div>
    </div> <!-- /.o_container -->
  </article>
</main>

<?php get_footer(); ?>

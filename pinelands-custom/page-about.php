<?php
/*
Template Name: Page - About
 */
get_header();
get_template_part('page','top');
?>

<main>
  <div class="o_container">
    <div class="o_row">
      <div class="o_col o_col__half">
        <figure class="c_figure">
          <img src="<?php echo get_template_directory_uri(); ?>/ui/459891361.jpg" alt="man doing custom wood work">
        </figure>
      </div>
      <div class="o_col o_col__half">
        <div class="c_description">

          <?php
          if (have_posts()) : while (have_posts()) : the_post();
            the_content();
          endwhile; endif;
           ?>

        </div>
      </div>
    </div>
  </div> <!-- /.o_container -->
</main>

<?php get_footer(); ?>

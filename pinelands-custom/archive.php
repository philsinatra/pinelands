<?php
get_header();
get_template_part('page','top');
?>

<main>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <article>
    <div class="o_container">
      <div class="o_row">
        <div class="o_col o_col__half">
          <figure class="c_figure">
            <?php
            if ( has_post_thumbnail() ) the_post_thumbnail();
            else {
              echo '<img src="' . get_bloginfo( 'stylesheet_directory' )
                  . '/ui/thumbnail-default.jpg" />';
            }
            ?>
          </figure>
        </div>
        <div class="o_col o_col__half">
          <div class="c_description">
            <h1><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
            <p><?php the_excerpt(); ?></p>
            <p>
              <a href="<?php the_permalink(); ?>" class="c_btn c_btn--dark">More</a>
            </p>
          </div>
        </div>
      </div>
    </div> <!-- /.o_container -->
  </article>

<?php endwhile; else: ?>
  <p>Some error message or similar.</p>
<?php endif; ?>

</main>

<?php get_footer(); ?>

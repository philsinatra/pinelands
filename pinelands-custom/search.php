<?php
get_header();
get_template_part('page','top');
?>

<main>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <article>
    <div class="o_container">
      <div class="o_row">
        <div class="o_col o_col__half">
          <figure class="c_figure">
            <img src="<?php echo get_template_directory_uri(); ?>/ui/459891361.jpg" alt="feature image">
          </figure>
        </div>
        <div class="o_col o_col__half">
          <div class="c_description">
            <h1><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
            <p><?php the_excerpt(); ?></p>
            <p>
              <a href="<?php the_permalink(); ?>" class="c_btn c_btn--dark">More</a>
            </p>
          </div>
        </div>
      </div>
    </div> <!-- /.o_container -->
  </article>

<?php endwhile; else: ?>
  <p>No results message.</p>
<?php endif; ?>


</main>

<?php get_footer(); ?>

$(function() {
  $(window).resize(function() {
    if ($(window).width() >= 768) {
      $('.o_header__nav').css('display', 'block');
    }
  });
  // Click event listeners
  $('.c_btn_menu').click(function(e) {
    e.preventDefault();
    $('.o_header__nav').slideToggle('.3s');
  });
});

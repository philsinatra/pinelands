<?php
require_once 'header.php';
// get_header();
?>

    <section class="o_hero">
      <div class="o_hero__promo">
        <div class="o_hero__promo_lead">
          <div class="o_container">
            <h1>Delivers Adjective Service</h1>
            <h2>For Home Owners and Businesses</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda quasi iure itaque quae voluptas harum blanditiis repudiandae facere fugiat corporis at est modi inventore, delectus, deserunt nemo dignissimos autem eaque.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et erat orci. Etiam in dolor at arcu scelerisque aliquet. Quisque lorem diam, volutpat in elementum non, facilisis vel ipsum. Fusce ut posuere neque. Vestibulum egestas, odio a gravida rhoncus, dolor mi ullamcorper velit, id lobortis nisl elit imperdiet neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
            <?php
            // Get the ID of a given category
            $category_id = get_cat_ID( 'Services' );

            // Get the URL of this category
            $category_link = get_category_link( $category_id );

             ?>
            <a href="<?php echo esc_url( $category_link ); ?>" title="Services" class="c_btn c_btn--dark">Our Services</a>
          </div>
          <!-- /.o_container -->
        </div>
        <!-- /.o_hero__promo_lead -->

          <div class="o_hero__promo_sub">
            <div class="o_container">
              <div class="o_row">
                <div class="o_col o_col__heading">
                  <div class="o_hero__promo_sub__heading">
                    <h1>Repair &amp; Design</h1>
                  </div>
                  <!-- /.o_hero__promo_sub__heading -->
                </div>
                <!-- /.o_col o_col__half -->
                <div class="o_col o_col__details">
                  <div class="o_hero__promo_sub__details">
                    <h2>Professional Contractor Services &amp; custom wood work</h2>
                    <p>Whether you&rsquo;re planning a renovation or simply need routine maintenance on your home. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa dolore aperiam laudantium, quos vitae laborum optio dolorum id a, adipisci repudiandae atque veniam ullam quibusdam itaque magnam quae accusantium! Ut!</p>
                  </div>
                  <!-- /.o_hero__promo_sub__details -->
                </div>
                <!-- /.o_col o_col__half -->
              </div>
              <!-- /.o_row -->
          </div>
          <!-- /.o_container -->
        </div>
        <!-- /.o_hero__promo_sub -->
      </div>
      <!-- /.hero__promo -->
    </section>
    <!-- /.hero -->

    <section class="o_index_services">
      <div class="o_container">
        <h1>Our Services</h1>
        <h2>What can we help you with today?</h2>
        <div class="o_row">
          <div class="o_col o_col__thirds">
            <div class="o_index_services__service">
              <figure><img src="<?php echo get_template_directory_uri(); ?>/ui/489036544.jpg" alt="remodeled kitchen"></figure>
              <div class="o_index_services__service-details">
                <h3>Residential Services</h3>
                <p>Service description lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt soluta magni cum asperiores quas animi vel odit fugiat facere at, quibusdam sint voluptate ducimus tempora laudantium tempore deserunt incidunt in!</p>
                <a href="http://www.pinelandsrepairanddesign.com/?s=residential" title="Residential Services" class="c_btn c_btn--small c_btn--dark">Read more</a>
              </div>
            </div>
            <!-- /.o_index_services__service -->
          </div>
          <!-- /.o_col o_col__thirds -->
          <div class="o_col o_col__thirds">
            <div class="o_index_services__service">
              <figure><img src="<?php echo get_template_directory_uri(); ?>/ui/464459747.jpg" alt="remodeled office"></figure>
              <div class="o_index_services__service-details">
                <h3>Commercial Services</h3>
                <p>Service description lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt soluta magni cum asperiores quas animi vel odit fugiat facere at, quibusdam sint voluptate ducimus tempora laudantium tempore deserunt incidunt in!</p>
                <a href="http://www.pinelandsrepairanddesign.com/?s=commercial" title="Commercial Services" class="c_btn c_btn--small c_btn--dark">Read more</a>
              </div>
            </div>
            <!-- /.o_index_services__service -->
          </div>
          <!-- /.o_col o_col__thirds -->
          <div class="o_col o_col__thirds">
            <div class="o_index_services__service">
              <figure><img src="<?php echo get_template_directory_uri(); ?>/ui/459892905.jpg" alt="man building custom table"></figure>
              <div class="o_index_services__service-details">
                <h3>Home Furnishing</h3>
                <p>Service description lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt soluta magni cum asperiores quas animi vel odit fugiat facere at, quibusdam sint voluptate ducimus tempora laudantium tempore deserunt incidunt in!</p>
                <?php
                // Get the ID of a given category
                $category_id = get_cat_ID( 'Home Furnishing' );

                // Get the URL of this category
                $category_link = get_category_link( $category_id );

                 ?>
                <a href="<?php echo esc_url( $category_link ); ?>" title="Home Furnishing" class="c_btn c_btn--small c_btn--dark">Read more</a>
              </div>
            </div>
            <!-- /.o_index_services__service -->
          </div>
          <!-- /.o_col o_col__thirds -->
        </div>
        <!-- /.o_row -->
      </div>
      <!-- /.o_container -->
    </section>
    <!-- /.o_index_services -->

    <section class="o_index_whatwhy">
      <div class="o_container">
        <div class="o_row">
          <div class="o_col o_col__half">
            <div class="o_whyus">
              <h2>Why Choose Us</h2>
              <div class="o_row">
                <div class="o_col o_col__half">
                  <figure><img src="<?php echo get_template_directory_uri(); ?>/ui/504860338.jpg" alt="design plan drawings"></figure>
                </div>
                <!-- /.o_col o_col__half -->
                <div class="o_col o_col__half">
                  <div class="o_whyus__details">
                    <h3>Precision Execution</h3>
                    <p>We offer every type of repair and remodeling solution for any type of issue.</p>
                  </div>
                  <!-- /.o_whyus__details -->
                </div>
                <!-- /.o_col o_col__half -->
              </div>
              <!-- /.o_row -->
              <div class="o_row">
                <div class="o_col o_col__half">
                  <figure><img src="<?php echo get_template_directory_uri(); ?>/ui/90262451.jpg" alt="remodeled kitchen"></figure>
                </div>
                <!-- /.o_col o_col__half -->
                <div class="o_col o_col__half">
                  <div class="o_whyus__details">
                    <h3>Not sure what you need?</h3>
                    <p>Let our expertise guide you in the design of your project. If a solution doesn't exist, we'll build one for you.</p>
                  </div>
                  <!-- /.o_whyus__details -->
                </div>
                <!-- /.o_col o_col__half -->
              </div>
              <!-- /.o_row -->
            </div>
            <!-- /.o_whyus -->
          </div>
          <!-- /.o_col o_col__half -->
          <div class="o_col o_col__half">
            <div class="o_clients">
              <h2>What our clients say</h2>
              <?php
              $testimonials = [
                'Jeff did a fantastic job on our master bath shower. His attention to detail in making sure it is done to perfection at a very reasonable price will make it a no brainier to work with Jeff again. I highly recommend!<br /><br />- Shaun Bodrog',
                'We used Pinelands R&amp;D to remodel our entire family room. From studs to trim Jeff did it all. We had a beautiful hand crafted bar created as well. Quality work, reliable, honest, a true craftsman. We will definitely be coming back when it\'s bathroom remodel time!<br /><br />- Melissa Sinatra',
                'Quality craftsmanship plus a great person to work with&hellip; Hire him! You won\'t regret it!<br /><br />-  Mechelle Lavelle',
                'Jeff is a very talented repairman, carpenter, you name it, I\'m sure he can fix it. Not only was the work done quickly, but the attention to detail was impeccable, and the pricing more than fair. Not to mention he\'s just a nice guy and a standup individual. I would highly recommend his services for pretty much whatever you need fixed.<br /><br />-Avi Steinhardt',
                'Jeff not only worked around our schedule but also completed the entire job in just 4 short days. He is one of the cleanest contractors I have ever used and will definitely use his expertise for any future jobs. His prices were by far the most reasonable out of all the quotes we received and his work was the most professional.<br /><br />-Vince DelleGrotti'
              ];
              $randomness = randomNumbers(0, count($testimonials) - 1, 2);
              for ($i=0; $i < 2; $i++) { ?>
                <blockquote>
                  <?php echo $testimonials[$randomness[$i]]; ?>
                </blockquote>
              <?php } ?>
            </div>
            <!-- /.o_clients -->
          </div>
          <!-- /.o_col o_col__half -->
        </div>
        <!-- /.o_row -->
      </div>
      <!-- /.o_container -->
    </section>
    <!-- /.o_index_whatwhy -->


<?php
include 'footer.php';
// get_footer();
 ?>

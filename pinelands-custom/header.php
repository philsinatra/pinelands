<!DOCTYPE html>
<html lang="en">
<head>
  <!-- TEMPLATE USED: <?php get_current_template( true ); ?> -->
  <meta charset="UTF-8">
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width, maximum-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
    <?php
        // The title
         wp_title( '|', true, 'right' );
        // Add the blog name.
        bloginfo( 'name' );
    ?>
  </title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Hind:600,300,700,400,500' rel='stylesheet' type='text/css'>
  <!-- <link rel="stylesheet" href="style.css"> -->
  <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php wp_head(); ?>

</head>
<body>

  <div class="o_wrapper">

    <header>
      <div class="o_container">
        <div class="o_header">

          <div class="o_header__brand">
            <picture>
              <a href="<?php echo get_option('home'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/ui/logo-v1.png" alt="Pinelands Repair and Design logo"></a>
            </picture>
          </div>
          <!-- /.o_header__brand -->

          <nav class="o_nav-header">
            <a href="#" class="c_btn_menu">Menu</a>
            <div class="o_header__nav is-hidden--mobile">
              <?php get_search_form(); ?>
              <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
            </div>
            <!-- /.o_header__nav -->
          </nav>
        </div>
        <!-- /.o_header -->
      </div>
      <!-- /.o_container -->
    </header>

<?php
/**
 * Template for displaying search forms
 * Inspired by: http://codepen.io/sdaitzman/pen/rmqfB
 */
 ?>

<form role="search" class="search-container" action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
  <input id="search-box" type="text" class="search-box" name="s" placeholder="Search&hellip;" required />
  <label for="search-box"><i class="search-icon fa fa-search"></i></label>
  <input type="submit" id="search-submit" />
</form>

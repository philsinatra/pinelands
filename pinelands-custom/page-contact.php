<?php
/*
Template Name: Page - Contact
 */
get_header();
get_template_part('page','top');
?>

<main>
  <div class="o_container">
    <div class="o_row">
      <div class="o_col o_col__half">
        <div class="c_description">
          <h2>We Would Love To Hear From You</h2>
          <p style="padding: 0">Need message here. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
        </div>
      </div>
      <div class="o_col o_col__half">
        <?php
        // TODO: CSS for form
        if (have_posts()) : while (have_posts()) : the_post();
          the_content();
        endwhile; endif;
         ?>
      </div>
    </div>
  </div> <!-- /.o_container -->
</main>

<?php get_footer(); ?>

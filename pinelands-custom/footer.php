<footer>
    <div class="o_footer__primary">
      <div class="o_container">
        <div class="o_row">
          <div class="o_col">
            <h4>About</h4>
            <figure class="o_footer__primary_logo">
              <img src="<?php echo get_template_directory_uri(); ?>/ui/logo-v1-reverse.png" alt="Pinelands Repair &amp; Design LLC logo" />
            </figure>
            <address>Pinelands Repair &amp; Design LLC<br />Medford, NJ, 08055<br /><a href="tel:609-247-7856">609-247-7856</a></address>
          </div>
          <!-- /.o_col -->
          <div class="o_col">
            <h4>Services</h4>
            <?php
              wp_nav_menu( array(
                'theme_location' => 'footer-menu',
                'menu_class' => 'c_services'
               ) );
            ?>
          </div>
          <!-- /.o_col -->
          <div class="o_col">
            <h4>Newsletter</h4>
            <p>Yes, I am interested in receiving the free newsletter which has recent information on plumbing and plumbing news for my home and my family. I prefer to receive the newsletter.</p>
            <!--
            // TODO Footer Contact Form Validation (does it work)
            -->
            <form action="">
              <!-- <input type="email" placeholder="Enter your email" required>
              <a href="#" class="c_btn c_btn-submit">subscribe</a> -->
              <?php echo do_shortcode('[contact-form-7 id="77" title="newsletter"]'); ?>
            </form>
          </div>
          <!-- /.o_col -->
          <div class="o_col">
            <h4>Service Coupon?</h4>
            <div class="c_coupon">
              <h1>$25 Off $100</h1>
              <h2>Specific Service?</h2>
              <p>Just mention this add.</p>
            </div>
            <!-- /.c_coupon -->
          </div>
          <!-- /.o_col -->
        </div>
        <!-- /.o_row -->
      </div>
      <!-- /.o_container -->
    </div>
    <!-- /.footer__primary -->

    <div class="o_footer__secondary">
      <div class="o_container">
        <div class="o_row">
          <div class="o_col">
            <div class="c_footer__secondary__social">
              <ul>
                <!--
                // TODO update with final social media links
                -->
                <li><a href="#facebook"><i class="fa fa-facebook-official"></i></a></li>
                <li><a href="#linkedin"><i class="fa fa-linkedin-square"></i></a></li>
                <li><a href="#instagram"><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
            <!-- /.o_footer__secondary__social -->
          </div>
          <!-- /.o_col -->
          <div class="o_col">
            <p class="c_copyright">&copy; 2015-<?php echo date("Y") ?> Pinelands Repair &amp; Design, LLC</p>
          </div>
          <!-- /.o_col -->
        </div>
        <!-- /.o_row -->
      </div>
      <!-- /.o_container -->
    </div>
    <!-- /.footer__secondary -->

</footer>

</div>
<!-- /.wrapper -->

<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js">

</script>

<?php wp_footer(); ?>

</body>
</html>

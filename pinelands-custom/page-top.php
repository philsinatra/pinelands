<div class="o_topofpage">
  <div class="o_container">
    <div class="o_row">
      <div class="c_page_title">
        <!--
        Conditionals to display title of either:
        page
        post
        search results
        category
        -->
        <h2>
          <?php
          if (is_archive()) {
            single_cat_title();
          }
          elseif (is_search()) {
            echo "Search for: " . get_search_query();
          }
          else the_title();
          ?>
        </h2>
      </div>
      <!-- <div class="c_breadcrumbs">
        breadcrumbs?
      </div> -->
    </div> <!-- /.o_row -->
  </div> <!-- /.o_container -->
</div> <!-- /.o_topofpage -->

# Pinelands Repair and Design

## WP Anatomy
- [https://codex.wordpress.org/Theme_Development](https://codex.wordpress.org/Theme_Development)
- [http://buildwpyourself.com/get-template-part/](http://buildwpyourself.com/get-template-part/)


- [/] stylesheet

Need to add theme header information to the top of the exported theme stylesheet file.

```css
/*
Theme Name: Twenty Thirteen
Theme URI: http://wordpress.org/themes/twentythirteen
Author: the WordPress team
Author URI: http://wordpress.org/
Description: The 2013 theme for WordPress takes us back to the blog, featuring a full range of post formats, each displayed beautifully in their own unique way. Design details abound, starting with a vibrant color scheme and matching header images, beautiful typography and icons, and a flexible layout that looks great on any device, big or small.
Version: 1.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: black, brown, orange, tan, white, yellow, light, one-column, two-columns, right-sidebar, flexible-width, custom-header, custom-menu, editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, translation-ready
Text Domain: twentythirteen

This theme, like WordPress, is licensed under the GPL.
Use it to make something cool, have fun, and share what you've learned with others.
*/
```

- [] functions.php (optional)
- [] header.php
- [] footer.php
- [] sidebar.php

### Template Files
- [] index.php
- [] home.php: default front page
- [] single.php
- [] page.php
- [] category.php
- [] search.php
- [] 404.php

## Content

Company Description:

Pinelands Repair and Design is a residential remodeling company that specializes in a client-centered approach.  We take great pride in our attention to detail and cleanliness during the course of your remodel.  Each project is built with the best and most suited materials for the application.  As a result, you are left with piece of mind that the work that is completed will last for generations, and you’ll experience timeless quality.  With Pinelands Repair and Design there is no pressure to do work that is unnecessary or tricky sales tactics.  Join a large group of homeowners that have continuously hired us time and again and entrusted us with their home remodeling and repair needs.

Home Services
- Remodeling
- Repair Work
- Electrical
- Plumbing
- Landscape Lighting
- Trimwork and Wainscot
- Drywall repairs
- Decks

Home Furnishings
- Tables – Farmhouse , Coffee, End Tables
- Outdoor Furniture
- Bars
- Storage solutions
- Organizational  shelving
- Mantels
- Bookshelves
- Built-In Shelving

Commercial Services
- Condominium association and professional office suite maintenance and repair
